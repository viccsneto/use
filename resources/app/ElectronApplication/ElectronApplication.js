const ElectronApplicationOptions = require('./ElectronApplicationOptions/ElectronApplicationOptions.js');
const IPCCommunicationLayer = require('./IPCCommunicationLayer/IPCCommunicationLayer.js');

const {
  app,
  BrowserWindow,
  ipcMain,
  nativeImage
} = require('electron');


app.disableHardwareAcceleration()

class ElectronApplication {
  constructor() {
    let applicationOptionsParser = new ElectronApplicationOptions();
    this._options = applicationOptionsParser.getApplicationOptions();
    this._ipcCommunicationLayer = new IPCCommunicationLayer(this._options.applicationOptions.nanoChannel);
    this._imageTexture = null;
    this._executeRequests = {
      counter: 0,
    };

    this._initializeMainWindow();
    this._initializeEventListeners();
  }

  _initializeMainWindow() {
    this._mainWindow = new BrowserWindow(this._options.mainWindowOptions);
    this._mainWindow.setResizable(true);
    this._mainWindow.loadURL(this._options.applicationOptions.urlAddress);
    this._mainWindow.webContents.frameRate = 30;
    this._mainWindow.webContents.setBackgroundThrottling(false);
  }

  _resizedEvent() {
    let dimensions = this._mainWindow.getSize();
    dimensions = {
      width: dimensions[0],
      height: dimensions[1]
    };

    if (this._options.mainWindowOptions.width !== dimensions.height || this._options.mainWindowOptions.height !== dimensions.height) {
      this._options.mainWindowOptions.width = dimensions.width;
      this._options.mainWindowOptions.height = dimensions.height;

      dimensions = JSON.stringify(dimensions);
      this._ipcCommunicationLayer.sendMessage("resized", dimensions);
    }
  }

  _initializeEventListeners()
  {
    this._registerApplicationListeners();
    this._registerIPCMainListeners();
    this._registerMainWindowListeners();
  }

  _registerMainWindowListeners()
  {
    this._mainWindow.webContents.on('before-input-event', (event, input) => {
      this._printDebugInfo("before-input-event", input);
    });

    this._mainWindow.webContents.on('after-input-event', (event, input) => {
      this._printDebugInfo("after-input-event", input);
    });



    this._mainWindow.webContents.on('resized', (event, newBounds) => {
      this._resizedEvent();
    });

    this._mainWindow.webContents.on('enter-full-screen', (event) => {
      this._resizedEvent();
    });

    this._mainWindow.webContents.on('leave-full-screen', (event) => {
      this._resizedEvent();
    });

    this._mainWindow.webContents.on('enter-html-full-screen', (event) => {
      this._resizedEvent();
    });

    this._mainWindow.webContents.on('leave-html-full-screen', (event) => {
      this._resizedEvent();
    });

    this._mainWindow.webContents.on('paint', (event, dirty, image) => {
      this._imageTexture = image;
      if (this._options.applicationOptions.force_paint_ready) {
        this._sendImageTexture();
      }
    });

    this._mainWindow.webContents.on('cursor-changed', (event, type) => {
      this._ipcCommunicationLayer.sendMessage("cursor-changed", type);
    });
  }

  _printDebugInfo() {
    if (this._options.applicationOptions.printDebugInfo) {
      console.log(arguments);
    }
  }

  async _execute(message) {
    try {
      this._printDebugInfo("Trying to execute: " + message.GetStringData());
      let execution_request = JSON.parse(message.GetStringData());

      let evaluatedFunction = eval("()=>{return " + execution_request.script + "}");
      if (evaluatedFunction) {
        try {
          evaluatedFunction.bind(this);
          let result = await evaluatedFunction();
          if (execution_request.callback_id !== 0) {

            let execution_response = {
              "callback_id" : execution_request.callback_id,
              "result": result
            };

            this.hostApplicationCallResult(JSON.stringify(execution_response));
          }

        } catch (err) {
          this._printDebugInfo("Failed to execute ", message.GetStringData() + " due to ", err);
        }
      } else {
        this._printDebugInfo("Failed to execute ", message.GetStringData());
      }
    } catch (err) {
      this._printDebugInfo("Failed to execute ", message.GetStringData() + " due to ", err);
    }
  }

  _call_result(message) {
    let response = message.GetStringData();
    this._printDebugInfo("Received call_result : " + response);
    this._mainWindow.webContents.send("call_result", response);
  }

  _ready() {
    this._sendImageTexture();
  }

  _destroy() {
    try {
      this._mainWindow.close();
    } catch (err) {
      this._printDebugInfo("Exception occurred while destroying object: " + err);
    }
  }

  _forwardMessageToMainWindow(message) {
    try {
      this._mainWindow.webContents.send(message.GetType(), message.GetStringData());
    } catch (err) {
      this._printDebugInfo("Exception occurred while forwarding custom message [" + message.GetType() + "] to mainWindow:\n" + err);
    }
  }

  _registerApplicationListeners() {
    this._ipcCommunicationLayer.addListener("execute", (message) => this._execute(message));
    this._ipcCommunicationLayer.addListener("call_result", (message) =>this._call_result(message));
    this._ipcCommunicationLayer.addListener("ready", (message) => this._ready(message));
    this._ipcCommunicationLayer.addListener("destroy", (message) => this._destroy(message));
    this._ipcCommunicationLayer.addListener("*", (message) => this._forwardMessageToMainWindow(message));
  }

  _registerIPCMainListeners() {
    ipcMain.on('Execute', (event, execute_object) => {
      this._printDebugInfo("ipcMain received Execute message from Window: " + JSON.stringify(execute_object));
      event.returnValue = this.hostApplicationExecute(execute_object);
    });

    ipcMain.on('BridgeExecute', (event, bridge_execute_object) => {
      this._printDebugInfo("ipcMain received BridgeExecute message from Window: " + JSON.stringify(bridge_execute_object));
      event.returnValue = this.hostApplicationBridgeExecute(bridge_execute_object);
    });

    ipcMain.on('HostApplicationCallResult', (event, execution_response) => {
      this.hostApplicationCallResult(execution_response);
      event.returnValue = true;
    });
  }

  _sendImageTexture() {
    if (this._imageTexture) {
      this._ipcCommunicationLayer.sendMessage("paint", this._imageTexture.getBitmap());
      this._imageTexture = null;
    }
  }

  hostApplicationExecute(script) {
    this._printDebugInfo("HostApplicationExecute called with arguments ", script);

    this._executeRequests.counter++;
    let request = {
      id: this._executeRequests.counter,
      script: script
    }

    let stringified_request = JSON.stringify(request);

    this._ipcCommunicationLayer.sendMessage("execute", stringified_request);

    return request.id;
  }

  hostApplicationBridgeExecute(bridge_request) {
    this._printDebugInfo("Execute called with arguments ", bridge_request);

    this._executeRequests.counter++;
    let request = {
      id: this._executeRequests.counter,
      ...bridge_request
    }

    let stringified_request = JSON.stringify(request);

    this._ipcCommunicationLayer.sendMessage("bridge-execute", stringified_request);

    return request.id;
  }

  hostApplicationCallResult(execution_response) {
    this._printDebugInfo("Trying to send call_result: " + execution_response);
    this._ipcCommunicationLayer.sendMessage("call_result", execution_response);
  }
}



let electronApplication;
function Init() {
  app.once('ready', () => {
      electronApplication = new ElectronApplication();
  });

  return app;
}

return Init();
