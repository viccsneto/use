const path = require('path')

const offscreen_rendering = false;

module.exports = {
  applicationOptions: {
    printDebugInfo: false,
    force_paint_ready: true,
    urlAddress: "about:blank",
  },
  mainWindowOptions: {
    show: !offscreen_rendering,
    frame: true,
    icon: path.join(__dirname, '../../assets/icons/png/64x64.png'),
    width: 800,
    height: 600,
    enableLargerThanScreen: true,
    skipTaskbar: true,
    transparent: false,
    webPreferences: {
      offscreen: offscreen_rendering,
      webSecurity: false,
      nodeIntegration: true,
      contextIsolation: false,
      preload: path.join(__dirname, '../Bridge/bridge_initialization_code.js')
    },
    resizable: true,
  }
}
