const defaultOptions = require('./DefaultOptions.js')
module.exports = class ElectronApplicationOptions {
  constructor()
  {
    this._options = {...defaultOptions}
    this._parseCommandLineOptions(process.argv);
  }


  getApplicationOptions() {
    return this._options;
  }

  _parseCommandLineOptions(args) {
    if (this._options.applicationOptions.printDebugInfo) {
      console.log("Commands args = " + args);
    }

    for (let i = 1; i < args.length; i++) {
      if (this._options.applicationOptions.printDebugInfo) {
        console.log("Processing args [" + i + "] = " + args[i]);
      }

      if (args[i] == "--channel") {
        this._options.applicationOptions.nanoChannel = args[i + 1];
      } else if (args[i] == "--url") {
        this._options.applicationOptions.urlAddress = args[i + 1];
      } else if (args[i] == "--width") {
        this._options.mainWindowOptions.width = Number(args[i + 1]);
      } else if (args[i] == "--height") {
        this._options.mainWindowOptions.height = Number(args[i + 1]);
      } else if (args[i] == "--transparent") {
        this._options.mainWindowOptions.transparent = true;
      } else if (args[i] == "--offscreen") {
        this._options.mainWindowOptions.webPreferences.offscreen = true;
        this._options.mainWindowOptions.show = false;
      }
    }
  }
}
