class HostApplication {
  constructor() {
    this._pending_callbacks = {};
    this._ipcRenderer = require("electron").ipcRenderer;
    this._initializeIPCEventHandlers();
  }

  _initializeIPCEventHandlers() {
    this._ipcRenderer.on("call_result", (event, call_result) => {
      let call_result_object = JSON.parse(call_result);
      let callback_function = this._pending_callbacks[call_result_object.id];
      if (callback_function) {
        callback_function(call_result_object.result);
        delete this._pending_callbacks[call_result_object.id];
      }
    });

    this._ipcRenderer.on("bridge-execute", (event, request) => {
      let request_object = JSON.parse(request);
      request_object.arguments = JSON.parse(request_object.arguments);
      if (bridge != null) {
        let result = bridge[request_object.name](...request_object.arguments);
        if (result) {
          let call_result = {
            callback_id: request_object.callback_id,
            result: result
          };
          this._ipcRenderer.sendSync("HostApplicationCallResult", JSON.stringify(call_result));
        }
      }
    });
  }

  GetIPCRenderer()
  {
    return this._ipcRenderer;
  }

  Execute(script, callback_function) {
    let callback_id = this._ipcRenderer.sendSync("Execute", script);
    if (callback_function) {
      this._pending_callbacks[callback_id] = callback_function;
    }
  }

  BridgeExecute(name, args, callback_function) {
    let callback_id = this._ipcRenderer.sendSync("BridgeExecute", {
      "name": name,
      "arguments": args
    });
    this._pending_callbacks[callback_id] = callback_function;
  }
};

hostApplication = new HostApplication();
bridge = {};

const hostapplicationLuaHandler = {
  get(target, prop, receiver) {
    if (target[prop]) {
      return target[prop];
    }

    function bridgedFunction() {
      let callback = null;
      let arguments_copy = [...arguments];
      if (arguments_copy.length > 0) {
        if (typeof (arguments_copy[arguments_copy.length - 1]) === "function") {
          callback = arguments_copy[arguments_copy.length - 1];
          arguments_copy = arguments_copy.slice(0, arguments_copy.length - 1);
        }
      }
      
      if (!callback) {
        let promise = new Promise((resolve, reject) => {
          target.BridgeExecute(prop, arguments_copy, (result) => {
            resolve(result);
          });
        });
        
        return promise;
      }
      
      target.BridgeExecute(prop, arguments_copy, callback)
    }

    return bridgedFunction
  }
};

hostApplication.Lua = new Proxy(hostApplication, hostapplicationLuaHandler);
