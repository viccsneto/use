const nanomsg = require('nanomsg');
const ElectronMessage = require('./ElectronMessage.js');

module.exports = class IPCCommunicationLayer {
  constructor(nanoChannel) {
    this._messageListeners = {};
    this._nanoChannel = 'ipc:///tmp/' + nanoChannel;
    this._nanoSocket = {};
    this._bindIPCSocket();
  }

  addListener(name, callback)
  {
    this._messageListeners[name] = callback;
  }

  sendMessage (msg_type, msg_data)
  {
    let electron_message = new ElectronMessage(msg_type, msg_data);
    electron_message.Send(this._nanoSocket);
  }

  _extractMessageType(buffer) {
    let type = "";
    let i = 0;
    while (i < buffer.length && buffer[i] != 0) {
      type += String.fromCharCode(buffer[i]);
      i++;
    }
    return type;
  }

  _nanoMessageToElectronMessage(buffer) {
    let type = this._extractMessageType(buffer);
    let type_offset = type.length + 1;
    let body = new Uint8Array(buffer.length - type_offset);

    for (let i = type_offset; i < buffer.length; ++i) {
      body[i - type_offset] = buffer[i];
    }

    let electron_message = new ElectronMessage(type, body);

    return electron_message;
  }

  _dispatchMessage(electron_message)
  {
    let listener = this._messageListeners[electron_message.GetType()];
    if (listener) {
      listener(electron_message);
    } else {
      let wildCardListener = this._messageListeners["*"];
      if (wildCardListener) {
        wildCardListener(electron_message)
      }
    }
  }

  _bindIPCSocket() {
    this._nanoSocket = nanomsg.socket('pair', {
      dontwait: true
    });
    this._nanoSocket.rcvmaxsize(-1);
    this._nanoSocket.bind(this._nanoChannel);

    this._nanoSocket.on('data', (buffer) => {
      let electron_message = this._nanoMessageToElectronMessage(buffer);
      this._dispatchMessage(electron_message);
    });
  }
}
