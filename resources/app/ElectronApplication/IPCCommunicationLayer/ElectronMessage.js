'use strict';

module.exports = class ElectronMessage
{
  constructor(type, msg)
  {
    this.m_msg_type = this.ConvertToString(type);
    this.m_msg_data = msg;
  }

  GetType()
  {
    return this.m_msg_type;
  }

  ConvertToString(buf)
  {
    let string_data = [];

    if (typeof(buf) == "string") {
      return buf;
    }
    else {
      for (let i = 0; i < buf.length && buf[i] != 0 && buf[i] != "\0"; i++) {
        string_data[i] = String.fromCharCode(buf[i]);
      }
    }

    return string_data.join('');
  }
  GetStringData()
  {
    return this.ConvertToString(this.GetData())
  }
  GetData()
  {
    return this.m_msg_data;
  }

  GetBuffer()
  {
    return this.m_msg_type.split('').concat(['\0'], this.m_msg_data).join('');
  }

  Send(nanosocket)
  {
    let type_size = this.m_msg_type.length;
    let body_offset = type_size + 1;
    let body_size = this.m_msg_data.length;
    let composed_data = new Uint8Array(type_size + 1 + body_size);

    for (let i = 0; i < type_size; ++i) {
      composed_data[i] = this.m_msg_type.charCodeAt(i);
    }

    composed_data[body_offset - 1] = 0;
    let composed_end_offset = body_offset + body_size;

    if (typeof(this.m_msg_data) == "string") {
      for (let i = body_offset; i < composed_end_offset; ++i) {
        composed_data[i] = this.m_msg_data.charCodeAt(i - body_offset);
      }
    }
    else {
      for (let i = body_offset; i < composed_end_offset; ++i) {
        composed_data[i] = this.m_msg_data[i - body_offset];
      }
    }

    nanosocket.send(composed_data);
  }
}

